var modal = document.querySelector(".modal");
var mask = document.querySelector(".modal-mask");

document.querySelector(".button").addEventListener("click", function() {
    modal.style.top = "0px";
    modal.style.opacity = "1";
    mask.style.display = "block";
    setTimeout( function() {
        mask.style.opacity = "1";
    }, 10)
});

var closeModal = function() {
    modal.style.top = "";
    modal.style.opacity = "";
    mask.style.opacity = "";
    setTimeout(function() {
        mask.style.display = "";
    }, 250);
}

document.querySelector(".close").addEventListener("click", closeModal);

document.querySelector(".modal-mask").addEventListener("click", closeModal);

var isEmailValid = function(email) {
    return email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z-0-9-]{1,67}\.[a-zA-Z]{2,}$/g);
}


var validateEmail = function() {
    var input = document.querySelector(".email");
    if(isEmailValid(input.value)) {
        input.style.background = "green";
        input.style.color = "white";
    } else {
        input.style.background = "rgb(255, 57, 67)";
        input.style.color = "";
    }
}

document.querySelector("form").addEventListener("submit", function(ev) {
    ev.preventDefault = true;
    var email = document.querySelector(".email");
    if(isEmailValid(email.value)) {
        document.querySelector(".email-invalid").style.display = "";
        document.querySelector(".email-envoye").style.display = "block";
    } else {
        document.querySelector(".email-invalid").style.display = "block";
        document.querySelector(".email-envoye").style.display = "";
    }
});
