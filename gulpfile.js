var gulp = require('gulp');
var gutil = require('gutil');
var sass = require('gulp-sass');
var sftp = require('gulp-sftp');
var babel = require('gulp-babel');
var htmlmin = require('gulp-htmlmin');
var combiner = require('stream-combiner2');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

gulp.task('sass-process', function(){
	var res = combiner.obj([
		gulp.src('src/**/*.scss'),
		sourcemaps.init(),
		sass({outputStyle: 'compressed'}),
		autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}),
		sourcemaps.write(''),
		gulp.dest('build/'),
		browserSync.stream()
	]);
	res.on('error', gutil.log);
	return res;
});

gulp.task('sass', ['sass-process'], function (done) {
	done();
});

gulp.task('js', function() {

	var res = combiner.obj([
		gulp.src("src/**/*.js"),
		sourcemaps.init(),
		babel({
			presets: [
				['@babel/env'],
				["minify"]
			]
		}),
		sourcemaps.write(''),
		gulp.dest('build/'),
	]);

	browserSync.reload();
	return res;
})

gulp.task('html', function() {
	var res = combiner.obj([
		gulp.src([
			"src/**/*.html",
			"src/**/*.svg"
		]),
		htmlmin({
			collapseWhitespace: true
		}),
		gulp.dest("build/")
	]);
	return res;
});

gulp.task('build', [ 'sass', 'js', 'html']);

gulp.task('deploy', ['build'], function() {
	return gulp.src('build/**/*')
		.pipe(sftp({
			host: 'sftp.url',
			user: 'user',
			pass: 'password',
			remotePath: '/home/deltawinbo/projects/windows'
		}));
});

gulp.task('default', ['build'], function() {
	browserSync.init({
		server: {
			baseDir: "build/"
		}
	});
	gulp.watch("src/**/*.sass", ['sass']);
	gulp.watch("src/**/*.js", ['js']);
});

gulp.task('stop', function() {
	process.exit();
});
